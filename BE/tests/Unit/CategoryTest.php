<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Book;
use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Schema;

class CategoryTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    
    use RefreshDatabase, WithFaker;
    
    /** @test */
    public function categories_table_has_expected_columns(){
        $this->assertTrue(
            Schema::hasColumns(
                'categories',
                [
                    'id',
                    'created_at',
                    'updated_at',
                    'category'
                ]
            )
        );
    }
    /** @test */
    public function category_belongs_to_a_book(){
        $category = Category::factory()->create();
        $book = Book::factory()->create(['category_id' => $category->id]);

        $this->assertInstanceOf(Category::class, $book->category);
    }
}
