<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\ReturnedBook;
use App\Models\BorrowedBook;
use App\Models\Category;
use App\Models\Book;
use App\Models\Patron;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Schema;

class PatronTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */

    use RefreshDatabase, WithFaker;

    /** @test */
    public function patrons_table_has_expected_columns(){
        $this->assertTrue(
            Schema::hasColumns(
                'patrons',
                [
                    'id',
                    'created_at',
                    'updated_at',
                    'last_name',
                    'first_name',
                    'middle_name',
                    'email'
                ]
            )
        );
    }
    /** @test */
    public function patron_belongs_to_many_returned_books(){
        $category = Category::factory()->create();
        $book = Book::factory()->create(['category_id' => $category->id]);
        $patron = Patron::factory()->create();
        $returnedBook = ReturnedBook::factory()->create(['patron_id' => $patron->id, 'book_id' => $book->id]);

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $returnedBook->patron);
    }
    /** @test */
    public function patron_belongs_to_many_borrowed_books(){
        $category = Category::factory()->create();
        $book = Book::factory()->create(['category_id' => $category->id]);
        $patron = Patron::factory()->create();
        $borrowedBook = BorrowedBook::factory()->create(['patron_id' => $patron->id, 'book_id' => $book->id]);

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $borrowedBook->patron);
    }
}
