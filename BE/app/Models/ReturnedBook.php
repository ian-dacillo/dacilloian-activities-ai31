<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReturnedBook extends Model
{
    use HasFactory;

    public function book(){
        return $this->hasMany(Book::class, 'id');
    }

    public function patron(){
        return $this->hasMany(Patron::class, 'id');
    }
}
