<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patron extends Model
{
    use HasFactory;

    public function borrowedBook(){
        return $this->belongsTo(BorrowedBook::class);
    }

    public function returnedBook(){
        return $this->belongsTo(ReturnedBook::class);
    }
}
