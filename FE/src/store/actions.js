import AXIOS from "../api/config";

export const getBooks = ({commit}) => {
    AXIOS.get('/books')
    .then(response => {
        commit('SET_BOOKS', response.data);
    })
};

export const getCategories = ({commit}) => {
    AXIOS.get('/categories')
    .then(response => {
        commit('SET_CATEGORIES', response.data);
    })
};

export const addBook = ({commit}, book) => {
    AXIOS.post('/books', book).then(response => {commit('ADD_BOOK', response.data)});
};

export const updateBook = ({commit}, book) => {
    AXIOS.put(`/books/${book.id}`, book).then(response => {commit('UPDATE_BOOK', response.data)});
}

export const getPatrons = ({commit}) => {
    AXIOS.get('/patrons')
    .then(response => {
        commit('SET_PATRONS', response.data);
    })
};

export const addPatron = ({commit}, patron) => {
    AXIOS.post('/patrons', patron).then(response => {commit('ADD_PATRON', response.data)});
};

export const updatePatron = ({commit}, patron) => {
    AXIOS.put(`/patrons/${patron.id}`, patron).then(response => {commit('UPDATE_PATRON', response.data)});
};

export const deletePatron = ({commit}, patron) => {
    AXIOS.delete(`/patrons/${patron.id}`, patron).then(response => {commit('DELETE_PATRON', response.data)});
}