var chart = document.getElementById('borrow-returnBooks').getContext('2d');

Chart.defaults.global.defaultFontSize = 15;
Chart.defaults.global.defaultFontStyle = 'bold'

var borrowedChart = new Chart(chart, {
    type:'bar',
    data:{
        labels:['January', 'February', 'March'],
        datasets:[
            {
                label:'Borrowed Books',
                data:[27, 92, 65],
                backgroundColor:'rgba(244,130,130)'
                }, 
            {
                label:'Returned Books',
                data:[53, 55, 34],
                backgroundColor: 'rgba(137,144,235)'
            },
            
        ],
        
    },
    options:{
        title:{
            display:true,
            text: 'Popular Books',
            fontSize:25,
            fontColor: '#000'
        },
    }
});

var chart2 = document.getElementById('popularBooks').getContext('2d');

var popularChart = new Chart(chart2, {
    type:'pie',
    data:{
        labels:['Fiction', 'Sci-Fi', 'History', 'Math'],
        datasets:[
            {
                label:'Borrowed Books',
                data:[45, 45, 45, 45],
                backgroundColor:
                [
                    'rgba(203,23,22)',
                    'rgba(91,180,238)',
                    'rgba(91, 184, 93)',
                    'rgba(255, 159, 64)'
                ]
            }
        ],
        
    },
    options:{
        title:{
            display:true,
            text: 'Popular Books',
            fontSize:25,
            fontColor: '#000'
        },
        legend:{
            
            position:'bottom'
        }
    }
    
});
